#!/bin/bash -e

docker_image_sans_version="ragnaroek/rust-raspberry"
docker_image_version="1.34.2"
docker_image="$docker_image_sans_version":"$docker_image_version"
src="$(pwd)/rust"
dst="/home/cross/project"
mount_arg="--mount type=bind,src=$src,dst=$dst"

# Taken from https://github.com/Ragnaroek/rust-on-raspberry-docker
run () {
    docker run $mount_arg "$docker_image" "$@"
}

debug () {
    docker run $mount_arg -it --entrypoint /bin/bash "$docker_image"
}

pull () {
    docker pull "$docker_image"
}

case "$1" in
    run)
        shift
        run "$@"
        ;;
    debug)
        debug
        ;;
    pull)
        pull
        ;;
    *)
        echo "Usage: $0 [run <cargo_command>]|debug|pull]"
        exit 1
esac
