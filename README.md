# Line Maze Model

## Setup

`./docker.sh pull`

## Build

`./docker.sh run build [--release]`

## Test

`./docker.sh run test`

## Debug

`./docker.sh debug`
