mod maze {
    pub const LINE_WIDTH: f64 = 0.25;
    pub const HALF_LINE_WIDTH: f64 = LINE_WIDTH / 2.0;
    pub const TEE_WIDTH: f64 = 2.0;
    pub const HALF_TEE_WIDTH: f64 = TEE_WIDTH / 2.0;
    pub const MINIMUM_LENGTH: f64 = 6.0;
    pub const ENDING_CIRCLE_RADIUS: f64 = 3.0;

    #[derive(PartialEq, Debug)]
    pub enum Color {
        Black,
        White,
    }

    #[derive(PartialOrd, PartialEq, Debug, Copy, Clone)]
    pub struct Position {
        pub x: f64,
        pub y: f64,
    }

    impl std::ops::Add<Position> for Position {
        type Output = Position;
        fn add(self, _rhs: Position) -> Position {
            Position {
                x: self.x + _rhs.x,
                y: self.y + _rhs.y,
            }
        }
    }

    impl std::ops::Sub<Position> for Position {
        type Output = Position;
        fn sub(self, _rhs: Position) -> Position {
            Position {
                x: self.x - _rhs.x,
                y: self.y - _rhs.y,
            }
        }
    }

    impl Position {
        pub fn new(_x: f64, _y: f64) -> Position {
            Position { x: _x, y: _y }
        }
    }

    pub trait Shape {
        fn is_point_inside(&self, point: Position) -> bool;
    }

    pub struct Rectangle {
        top_left_corner: Position,
        bottom_right_corner: Position,
    }

    impl Shape for Rectangle {
        fn is_point_inside(&self, point: Position) -> bool {
            point.x >= self.top_left_corner.x
                && point.x <= self.bottom_right_corner.x
                && point.y >= self.bottom_right_corner.y
                && point.y <= self.top_left_corner.y
        }
    }

    impl Rectangle {
        pub fn new(top_left: Position, bottom_right: Position) -> Rectangle {
            Rectangle {
                top_left_corner: top_left,
                bottom_right_corner: bottom_right,
            }
        }
    }

    pub struct Circle {
        radius: f64,
        center: Position,
    }

    impl Shape for Circle {
        fn is_point_inside(&self, point: Position) -> bool {
            let delta_x = point.x - self.center.x;
            let delta_y = point.y - self.center.y;
            self.radius.powf(2.0) >= delta_x.powf(2.0) + delta_y.powf(2.0)
        }
    }

    impl Circle {
        pub fn new(_radius: f64, _center: Position) -> Circle {
            Circle {
                radius: _radius,
                center: _center,
            }
        }
    }

    fn is_point_on_maze(point: Position, maze: &Vec<Box<dyn Shape>>) -> bool {
        let mut on_maze = false;
        for shape in maze.iter() {
            on_maze = on_maze || shape.is_point_inside(point);
        }
        on_maze
    }

    pub struct Floor {
        maze: Vec<Box<dyn Shape>>,
    }

    impl Floor {
        pub fn new(_maze: Vec<Box<dyn Shape>>) -> Floor {
            Floor { maze: _maze }
        }

        pub fn get_color(&self, pos: Position) -> Color {
            if is_point_on_maze(pos, &self.maze) {
                Color::Black
            } else {
                Color::White
            }
        }
    }

    #[cfg(test)]
    mod test {
        use super::*;

        const EPSILON: f64 = 0.01;

        pub(crate) fn create_simplest_floor() -> Floor {
            let tee = Rectangle {
                top_left_corner: Position {
                    x: -HALF_LINE_WIDTH,
                    y: HALF_TEE_WIDTH,
                },
                bottom_right_corner: Position {
                    x: HALF_LINE_WIDTH,
                    y: -HALF_TEE_WIDTH,
                },
            };

            let first_segment = Rectangle {
                top_left_corner: Position {
                    x: HALF_LINE_WIDTH,
                    y: HALF_LINE_WIDTH,
                },
                bottom_right_corner: Position {
                    x: HALF_LINE_WIDTH + MINIMUM_LENGTH,
                    y: -HALF_LINE_WIDTH,
                },
            };

            let ending_circle = Circle {
                radius: ENDING_CIRCLE_RADIUS,
                center: Position {
                    x: HALF_LINE_WIDTH + MINIMUM_LENGTH + ENDING_CIRCLE_RADIUS,
                    y: 0.0,
                },
            };

            let maze: Vec<Box<dyn Shape>> = vec![
                Box::new(tee),
                Box::new(first_segment),
                Box::new(ending_circle),
            ];
            Floor::new(maze)
        }

        #[test]
        fn slightly_right_of_tee_top_right_is_white() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH + EPSILON,
                y: HALF_TEE_WIDTH,
            };
            assert_eq!(Color::White, floor.get_color(test_point));
        }

        #[test]
        fn slightly_above_tee_top_right_is_white() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH,
                y: HALF_TEE_WIDTH + EPSILON,
            };
            assert_eq!(Color::White, floor.get_color(test_point));
        }

        #[test]
        fn slightly_left_of_tee_top_right_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH - EPSILON,
                y: HALF_TEE_WIDTH,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_below_tee_top_right_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH,
                y: HALF_TEE_WIDTH - EPSILON,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_right_of_tee_top_left_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: -HALF_LINE_WIDTH + EPSILON,
                y: HALF_TEE_WIDTH,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_above_tee_top_left_is_white() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: -HALF_LINE_WIDTH,
                y: HALF_TEE_WIDTH + EPSILON,
            };
            assert_eq!(Color::White, floor.get_color(test_point));
        }

        #[test]
        fn slightly_left_of_tee_top_left_is_white() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: -HALF_LINE_WIDTH - EPSILON,
                y: HALF_TEE_WIDTH,
            };
            assert_eq!(Color::White, floor.get_color(test_point));
        }

        #[test]
        fn slightly_below_tee_top_left_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: -HALF_LINE_WIDTH,
                y: HALF_TEE_WIDTH - EPSILON,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_right_of_tee_bottom_right_is_white() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH + EPSILON,
                y: -HALF_TEE_WIDTH,
            };
            assert_eq!(Color::White, floor.get_color(test_point));
        }

        #[test]
        fn slightly_above_tee_bottom_right_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH,
                y: -HALF_TEE_WIDTH + EPSILON,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_left_of_tee_bottom_right_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH - EPSILON,
                y: -HALF_TEE_WIDTH,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_below_tee_bottom_right_is_white() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH,
                y: -HALF_TEE_WIDTH - EPSILON,
            };
            assert_eq!(Color::White, floor.get_color(test_point));
        }

        #[test]
        fn slightly_right_of_tee_bottom_left_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: -HALF_LINE_WIDTH + EPSILON,
                y: -HALF_TEE_WIDTH,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_above_tee_bottom_left_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: -HALF_LINE_WIDTH,
                y: -HALF_TEE_WIDTH + EPSILON,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_left_of_tee_bottom_left_is_white() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: -HALF_LINE_WIDTH - EPSILON,
                y: -HALF_TEE_WIDTH,
            };
            assert_eq!(Color::White, floor.get_color(test_point));
        }

        #[test]
        fn slightly_below_tee_bottom_left_is_white() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: -HALF_LINE_WIDTH,
                y: -HALF_TEE_WIDTH - EPSILON,
            };
            assert_eq!(Color::White, floor.get_color(test_point));
        }

        #[test]
        fn center_of_tee_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position { x: 0.0, y: 0.0 };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_right_of_first_segment_top_left_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH + EPSILON,
                y: HALF_LINE_WIDTH,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_above_first_segment_top_left_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH,
                y: HALF_LINE_WIDTH + EPSILON,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_left_of_first_segment_top_left_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH - EPSILON,
                y: HALF_LINE_WIDTH,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_below_first_segment_top_left_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH,
                y: HALF_LINE_WIDTH - EPSILON,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_right_and_above_first_segment_top_left_is_white() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH + EPSILON,
                y: HALF_LINE_WIDTH + EPSILON,
            };
            assert_eq!(Color::White, floor.get_color(test_point));
        }

        #[test]
        fn slightly_right_of_first_segment_bottom_left_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH + EPSILON,
                y: -HALF_LINE_WIDTH,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_above_first_segment_bottom_left_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH,
                y: -HALF_LINE_WIDTH + EPSILON,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_left_of_first_segment_bottom_left_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH - EPSILON,
                y: -HALF_LINE_WIDTH,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_below_first_segment_bottom_left_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH,
                y: -HALF_LINE_WIDTH - EPSILON,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn slightly_right_and_below_first_segment_bottom_left_is_white() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH + EPSILON,
                y: -HALF_LINE_WIDTH - EPSILON,
            };
            assert_eq!(Color::White, floor.get_color(test_point));
        }

        #[test]
        fn center_of_finish_is_black() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH + MINIMUM_LENGTH + ENDING_CIRCLE_RADIUS,
                y: 0.0,
            };
            assert_eq!(Color::Black, floor.get_color(test_point));
        }

        #[test]
        fn perimeter_of_finish_is_black() {
            let floor = create_simplest_floor();
            let test_point_x = Position {
                x: HALF_LINE_WIDTH + MINIMUM_LENGTH + 2.0 * ENDING_CIRCLE_RADIUS,
                y: 0.0,
            };
            let test_point_y = Position {
                x: HALF_LINE_WIDTH + MINIMUM_LENGTH + ENDING_CIRCLE_RADIUS,
                y: ENDING_CIRCLE_RADIUS,
            };

            assert_eq!(Color::Black, floor.get_color(test_point_x));
            assert_eq!(Color::Black, floor.get_color(test_point_y));
        }

        #[test]
        fn slightly_above_finish_is_white() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH + MINIMUM_LENGTH + ENDING_CIRCLE_RADIUS,
                y: ENDING_CIRCLE_RADIUS + EPSILON,
            };

            assert_eq!(Color::White, floor.get_color(test_point));
        }

        #[test]
        fn slightly_below_finish_is_white() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH + MINIMUM_LENGTH + ENDING_CIRCLE_RADIUS,
                y: -ENDING_CIRCLE_RADIUS - EPSILON,
            };

            assert_eq!(Color::White, floor.get_color(test_point));
        }

        #[test]
        fn slightly_right_of_finish_is_white() {
            let floor = create_simplest_floor();
            let test_point = Position {
                x: HALF_LINE_WIDTH + MINIMUM_LENGTH + 2.0 * ENDING_CIRCLE_RADIUS + EPSILON,
                y: 0.0,
            };

            assert_eq!(Color::White, floor.get_color(test_point));
        }

    }
}

use maze::Position;

pub struct Sensor {
    body_relative_pos: Position,
}

impl Sensor {
    pub fn new(body_pos: Position) -> Sensor {
        Sensor {
            body_relative_pos: body_pos,
        }
    }
}

pub struct Robot {
    location: Position,
    orientation: f64,
    sensors: Vec<Sensor>,
}

impl Robot {
    pub fn new(origin: Position, angle: f64, _sensors: Vec<Sensor>) -> Robot {
        Robot {
            location: origin,
            orientation: angle,
            sensors: _sensors,
        }
    }

    pub fn translate(&mut self, distance: f64) {
        self.location.x += distance * self.orientation.cos();
        self.location.y += distance * self.orientation.sin();
    }

    pub fn turn(&mut self, angle: f64) {
        self.orientation += angle;
    }

    pub fn location(&self) -> Position {
        self.location
    }

    pub fn orientation(&self) -> f64 {
        self.orientation
    }

    fn rotate(&self, body_pos: Position) -> Position {
        rotate_body_position(body_pos, self.orientation)
    }
}

fn rotate_body_position(pos: Position, angle: f64) -> Position {
    let r = (pos.x * pos.x + pos.y * pos.y).sqrt();
    Position {
        x: r * angle.cos(),
        y: angle.sin(),
    }
}

pub(crate) mod test_api {
    use crate::maze::*;

    pub(crate) fn create_simplest_floor() -> Floor {
        let tee = Rectangle::new(
            Position {
                x: -HALF_LINE_WIDTH,
                y: HALF_TEE_WIDTH,
            },
            Position {
                x: HALF_LINE_WIDTH,
                y: -HALF_TEE_WIDTH,
            },
        );

        let first_segment = Rectangle::new(
            Position {
                x: HALF_LINE_WIDTH,
                y: HALF_LINE_WIDTH,
            },
            Position {
                x: HALF_LINE_WIDTH + MINIMUM_LENGTH,
                y: -HALF_LINE_WIDTH,
            },
        );

        let ending_circle = Circle::new(
            ENDING_CIRCLE_RADIUS,
            Position {
                x: HALF_LINE_WIDTH + MINIMUM_LENGTH + ENDING_CIRCLE_RADIUS,
                y: 0.0,
            },
        );

        let maze: Vec<Box<dyn Shape>> = vec![
            Box::new(tee),
            Box::new(first_segment),
            Box::new(ending_circle),
        ];
        Floor::new(maze)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::f64::consts::PI;

    fn assert_position_equal(p1: Position, p2: Position) {
        const NORM_EPSILON: f64 = 1e-10;
        let delta_pos = p1 - p2;
        assert!(delta_pos.x.powf(2.0) + delta_pos.y.powf(2.0) < NORM_EPSILON);
    }

    fn assert_float_equal(f1: f64, f2: f64) {
        const FLOAT_EPSILON: f64 = 1e-10;
        let delta_f = f1 - f2;
        assert!(delta_f.abs() < FLOAT_EPSILON)
    }

    #[test]
    fn moves_forward_by_translation() {
        let mut robot = Robot::new(Position::new(0.0, 0.0), 0.0, vec![]);
        robot.translate(1.0);
        assert_position_equal(Position::new(1.0, 0.0), robot.location());
    }

    #[test]
    fn moves_backward_by_negative_translation() {
        let mut robot = Robot::new(Position::new(0.7, 0.5), PI / 2.0, vec![]);
        robot.translate(-2.0);
        assert_position_equal(Position::new(0.7, -1.5), robot.location());
    }

    #[test]
    fn robot_changes_orientation_by_provided_angle() {
        let mut robot = Robot::new(Position::new(0.0, 0.0), 1.0, vec![]);
        robot.turn(0.7);
        assert_float_equal(robot.orientation(), 1.7);
    }

}
